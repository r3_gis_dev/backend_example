<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class HelloWorldController
{
    /**
    * @Route("/{name}", defaults={"name": null})
    * @Method("GET")
    */
    public function index($name)
    {
        if (is_null($name)) {
            $name = 'world';
        }
        $response = new JsonResponse([
            'message' => sprintf('Hello %s', $name)
        ]);

        return $response;
    }
}
