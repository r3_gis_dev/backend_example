
Steps:

```

composer init
composer require symfony/flex
composer require symfony/skeleton
composer require sensio/framework-extra-bundle
composer require symfony/orm-pack
composer require symfony/maker-bundle --dev
composer require symfony/dotenv
composer require symfony/web-server-bundle --dev

php bin/console   server:run

```
